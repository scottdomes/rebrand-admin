/* eslint-disable */

import page from 'page'
import React from 'react'
import ReactDOM from 'react-dom'

import App from '../app'

const container = document.getElementById('root')

page('/', function() {
  ReactDOM.render(
    <App />, 
    container
  )
})



// setup the 404 handler
page('*', () => { page.redirect('/') })

// start the router
page.start()
