import React, { Component } from 'react';
import Sidebar from './nav/sidebar'
import Header from './nav/header'
import './App.scss';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Sidebar/>
        <Header campaignTitle={this.props.campaignTitle}/>
      </div>
    );
  }
}

export default App;
