/* eslint-disable */

import './config'

// Importing bootstrap and font-awesome here.
import 'bootstrap/dist/css/bootstrap.css'
import 'imports?jQuery=jquery,$=jquery,this=>window!bootstrap/dist/js/bootstrap'
// import 'font-awesome/css/font-awesome.css'
// import  './vendor/rollbar.js'
// import './vendor/mouseflow.js'

// import './styles/main.scss'
import './fonts/mdi/css/materialdesignicons.min.css'

import './init'
import './router/router'
