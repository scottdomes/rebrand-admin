  import $ from 'jquery'
  import React, {Component} from 'react'
  import {observer} from 'mobx-react'
  import './styles/circle_icon.scss'
  
  @observer
  export default class CircleIcon extends Component{
    componentDidMount() {
      $('[data-toggle="tooltip"]').tooltip()
    }

    render() {
      return ( 
        <div className="circle-icon" data-toggle="tooltip" data-placement="right" title={this.props.tooltip}>
          <i className={"mdi mdi-" + this.props.icon}/>
        </div>
      )
    }
  }
  
  CircleIcon.propTypes = {
    icon: React.PropTypes.string.isRequired,
    tooltip: React.PropTypes.string.isRequired
  }