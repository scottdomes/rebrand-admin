/* eslint-disable */

import $ from 'jquery'
import UiResource from './resources/ui_resource'

$.fn.serializeObject = function () {
  var o = {};
  var a = this.serializeArray();
  $.each(a, function () {
    if (o[this.name] !== undefined) {
      if (!o[this.name].push) {
        o[this.name] = [o[this.name]];
      }
      o[this.name].push(this.value || '');
    } else {
      o[this.name] = this.value || '';
    }
  });
  return o;
}

$.ajaxSetup({headers: {'Authorization': "Basic " + localStorage.getItem('token')}})

$(document).ajaxError(function (data, event) {
  console.log(event)
  try {
    if (event.responseJSON.status === 403 && window.location.pathname !== '/login' && window.location.pathname !== '/onboard') {
      window.location.href = '/login'
    }
    var error = $.parseJSON(event.responseText)
    UiResource.notify('danger', 'Error: ' + error.status + ' ' + error.error + '. ' + error.exception.replace(/(#|<|>)/g, ''))
    
  } catch (err) {
  }
})


// todo: fix this by setting cookies in the browser
try {
  let token = window.location.search.split('?')[1].split('token=')[1]
  if (token) {
    localStorage.setItem('token', token)
    window.location.href = '/'
  }
} catch (e) { }
