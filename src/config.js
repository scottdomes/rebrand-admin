/* eslint-disable */

const configuration = {
  production: {
    HOMEPAGE:     'https://musefind.com',
    COLLABS_API:  'https://collabs-api.musefind.com',
    USERS_API:    'https://users-api.musefind.com',
    NOVA_API:     'https://nova-api.musefind.com',
    SIGNUP_API:   'https://api.musefind.com/signup-api',
    CHAT_API:     'https://api.musefind.com/chat-api',
    DEV_MODE: false
  },
  development: {
    HOMEPAGE:     'https://staging.musefind.com',
    NOVA_API:     'https://nova-api.musefind.com',
    COLLABS_API:  'http://staging.musefind.com:4000',
    USERS_API:    'http://staging.musefind.com:3000',
    SIGNUP_API:   'http://staging.musefind.com:4567',
    CHAT_API:     'http://staging.musefind.com:8080',
    DEV_MODE: true
  },
  staging: {
    HOMEPAGE:     'https://staging.musefind.com',
    NOVA_API:     'https://nova-api.musefind.com',
    COLLABS_API:  'http://staging.musefind.com:4000',
    USERS_API:    'http://staging.musefind.com:3000',
    SIGNUP_API:   'http://staging.musefind.com:4567',
    CHAT_API:     'http://staging.musefind.com:8080',
    DEV_MODE: true
  },
  demo: {
    HOMEPAGE:     'https://demo.musefind.com',
    NOVA_API:     'https://nova-api.musefind.com',
    COLLABS_API:  'http://demo.musefind.com:4000',
    USERS_API:    'http://demo.musefind.com:3000',
    SIGNUP_API:   'http://demo.musefind.com:4567',
    CHAT_API:     'http://demo.musefind.com:8080',
    DEV_MODE: true
  }
}


// const config = configuration[process.env.NODE_ENV || 'development']
const config = configuration['production']
window.config = config
export default config
