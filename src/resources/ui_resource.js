/* eslint-disable */


export default class UIResource { 
  static fixBrokenImage(e) {
    e.target.src = require('../images/logos/logo-single-white.png')
  }

  static roundStat(stat, roundTo=1000) {
    if (stat > 0) {
      return (stat/roundTo).toFixed(1) + 'K'
    } else {
      return 0
    }
  }

  static formatTag(str) {
    if (str.indexOf('#') === -1) {
      return '#'.concat(str)
    } else {
      return str
    }
  }

  static formatReach(reach) {
    if (reach > 1000000) {
      return ((reach / 1000000).toFixed(2) + 'M')
    } else if (reach > 1000) {
      return ((reach / 1000).toFixed(0) + 'K')
    } else if (reach === null || reach === undefined) {
      return '0'
    } else {
      return reach.toString()
    }
  }

  static formatPrice(price) {
    return '$' + parseInt(price).toFixed(2)
  }

  static sanitizeInput(string) {
    return string.replace(/[^a-z0-9áéíóúñü \/\:\?\'\n\.,_-]/gim, '').replace(/^0+/, '')
  }


}