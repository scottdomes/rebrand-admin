import React, {Component} from 'react'
import {observer} from 'mobx-react'
import './styles/header.scss'

@observer
export default class Header extends Component{
  render() {
    return ( 
      <div id="header">
        <h2>{this.props.campaignTitle}</h2>
      </div>
    )
  }
}

Header.propTypes = {
  id: React.PropTypes.number
}