import $ from 'jquery'
import React, {Component} from 'react'
import {observer} from 'mobx-react'
import './styles/sidebar.scss'

import CircleIcon from '../icons/circle_icon'

@observer
export default class Sidebar extends Component {  
  tooltipsEnabled = true

  showCampaignDetails(e) {
    e.stopPropagation()
    if (this.tooltipsEnabled) {
      $('.circle-icon').tooltip('destroy')
      $('#campaign-details').tooltip('show')
      this.tooltipsEnabled = false
    } else {
      $('.circle-icon').tooltip()
      $('#campaign-details').tooltip('hide')
      this.tooltipsEnabled = true
    }
  }

  render() {
    return ( 
      <div id="sidebar">
        <img alt="MuseFind Logo" src={require('../images/logos/logo-emblem-white.png')}/>
        <div id="icons">
          <div 
            id="campaign-details" 
            onClick={this.showCampaignDetails.bind(this)} 
            data-toggle="tooltip" 
            data-placement="right" 
            title="
              <h4>0 of 100k</h4>
              <p>Target Impressions</p>
              <h4>0</h4>
              <p>Potential Impressions</p>
              <h4>$150 of $500</h4>
              <p>Budget Spent</p>"
            data-html="true"
            data-container="#campaign-details-tooltip-container"
            data-trigger="manual">
              <CircleIcon icon="rocket" tooltip="Campaign Info"/>
            <div id="campaign-details-tooltip-container"></div>
          </div>
          <CircleIcon icon="eye" tooltip="View as brand"/>
          <CircleIcon icon="content-copy" tooltip="Templates"/>
        </div>
      </div>
    )
  }
}

Sidebar.propTypes = {
  id: React.PropTypes.number
}